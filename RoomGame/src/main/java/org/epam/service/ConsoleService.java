package org.epam.service;

import org.springframework.stereotype.Component;

@Component
public class ConsoleService {
    public void introdeceConsole() {
        System.out.println("You have 10 different doors to choose:\n" +
                "-3 of them with Artifacts which can give you an extra power;\n" +
                "-rest of them with monster (every monster will have 10-100 power) which you need to battle;\n" +
                "-your start power is 25;\n" +
                "-you can beat monster if your power equals or higher than power of monster;\n" +
                "GOOD LUCK!!!");
    }
}
