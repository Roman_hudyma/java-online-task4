package org.epam.util;

import org.springframework.stereotype.Component;

@Component
public class BattleUtil {
    public void battleMonster(int heroPower, int monsterPower)
    {
        System.out.println("Your power:"+heroPower+"  VS  Monster power:"+monsterPower);
        System.out.println("Battling...");
        try {
            Thread.sleep(2000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        if(heroPower>=monsterPower)
        {
            System.out.println("You fight down this monster!!!");
        }
        else{
            System.out.println("YOU DIED!!!");
        }
    }
}
