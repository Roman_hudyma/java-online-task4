package org.epam;

import org.epam.common.Door;
import org.epam.common.Hero;
import org.epam.service.ConsoleService;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan(basePackages =
        {"org.epam.common",
                "org.epam.service," +
                        "org.epam.util"})
@SpringBootApplication
public class RoomsApllicationRunner {
    public static void main(String[] args) {
        ConfigurableApplicationContext context = SpringApplication.run(RoomsApllicationRunner.class);
        Door door = (Door) context.getBean("door");
        ConsoleService service = (ConsoleService) context.getBean("consoleService");
        Hero hero = (Hero) context.getBean("hero");
        for (int i = 0; i < 10; i++) {
            service.introdeceConsole();
            door.getDoors();
            door.chooseDoor();

        }
        context.close();
    }
}
