package org.epam.common;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class Monster {
    public int[] monsterMassive = new int[7];

}
