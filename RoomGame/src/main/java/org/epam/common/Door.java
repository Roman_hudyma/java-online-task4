package org.epam.common;

import org.springframework.stereotype.Component;

import java.util.Scanner;

@Component("door")
public class Door {
Scanner scanner = new Scanner(System.in);
    private String[] doorsMassive = new String[]{
            "  ________      1\n" +
                    " /        \\  \n" +
                    "/    /\\    \\ \n" +
                    "|    \\/     |  \n" +
                    "|           |\n" +
                    "|        ═╗ |\n" +
                    "|           |\n" +
                    "|           |\n" +
                    "|___________|",
            "  ________     2\n" +
                    " /        \\  \n" +
                    "/    ____  \\ \n" +
                    "|   |____|  | \n" +
                    "|           | \n" +
                    "|        ═╗ | \n" +
                    "|           | \n" +
                    "|           | \n" +
                    "|___________| \n",
            "  *********      3\n" +
                    " *         * \n" +
                    "*    ----    *\n" +
                    "*   *||||*   *\n" +
                    "*    ----    *\n" +
                    "*        ═╗  *\n" +
                    "*            *\n" +
                    "*            *\n" +
                    "**************",
            "  ♦♦♦♦♦♦♦♦♦      4\n" +
                    " ♦          ♦ \n" +
                    "♦   ✠   ✠   ♦\n" +
                    "♦   ✠   ✠   ♦\n" +
                    "♦            ♦\n" +
                    "♦        ═╗  ♦\n" +
                    "♦            ♦\n" +
                    "♦            ♦\n" +
                    "♦♦♦♦♦♦♦♦♦♦♦♦♦\n",
            "    _____         5\n" +
                    "  ◢      ◣  \n" +
                    "◢          ◣\n" +
                    "❚   ☯   ☯   ❚\n" +
                    "❚            ❚\n" +
                    "❚        ═╗  ❚\n" +
                    "❚            ❚\n" +
                    "❚            ❚\n" +
                    "❚____________❚\n",
            "   _________      6\n" +
                    "  ◆        ◆ \n" +
                    " ◆          ◆\n" +
                    "◀      ☾☽    ▶\n" +
                    "◀      ☾☽    ▶\n" +
                    "◀         ═╗ ▶\n" +
                    "◀            ▶\n" +
                    "◀            ▶\n" +
                    "◀____________▶\n",
            "   _________      7\n" +
                    "  ◊        ◊ \n" +
                    " ◊          ◊ \n" +
                    "◊     ☠     ◊\n" +
                    "◊     ☠     ◊\n" +
                    "◊         ═╗ ◊\n" +
                    "◊            ◊\n" +
                    "◊            ◊\n" +
                    "◊____________◊\n",
            "   ________        8\n" +
                    "  ●        ● \n" +
                    " ●          ●\n" +
                    "◘    ░ ░    ◘\n" +
                    "◘    ░ ░    ◘\n" +
                    "◘         ═╗ ◘\n" +
                    "◘            ◘\n" +
                    "◘            ◘\n" +
                    "◘____________◘\n",
            "   _________       9\n" +
                    "  ◈        ◈\n" +
                    " ◈          ◈\n" +
                    "◈    █  █   ◈\n" +
                    "◈    █  █   ◈\n" +
                    "◈         ═╗ ◈\n" +
                    "◈            ◈\n" +
                    "◈            ◈\n" +
                    "◈____________◈\n",
            "   ________       10\n" +
                    "  ⋐        ⋑ \n" +
                    " ⋐   ____   ⋑\n" +
                    "⋐   |____|   ⋑\n" +
                    "⋐            ⋑\n" +
                    "⋐         ═╗ ⋑\n" +
                    "⋐            ⋑\n" +
                    "⋐            ⋑\n" +
                    "⋐____________⋑\n"
    };

    public int chooseDoor()
    {
        System.out.println("Choose one of exist doors:");
        int choose=scanner.nextInt();
        if(choose>doorsMassive.length){
            System.out.println("THIS DOOR DON'T EXIST");
        }
        return choose;
    }
    public void getDoors() {
        for (int i = 0; i < doorsMassive.length; i++) {
            System.out.println(doorsMassive[i]);
        }
    }
    public void deleteDoors()
    {

    }

}
