package org.epam.common;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope("singleton")
public class Hero {
    private int heroPower=25;

    public int getHeroPower() {
        return heroPower;
    }

    public int addHeroPower(int artifactPower) {
        return this.heroPower = this.heroPower + artifactPower;
    }

}
